��    F      L  a   |                      (     .  	   2     <     B     I     Z     b     i     o     �     �     �     �     �     �  
   �  	   �     �     �       -   *     X     `     g  	   z     �     �     �  	   �  
   �     �     �     �     �     �     �     �  	             (     .  
   :     E     U     a     h     ~     �     �  	   �     �     �  	   �     �     �     �     �     �  
   	     	     	     	     '	     3	     :	     I	  9  \	     �
     �
     �
     �
     �
     �
                  	   ,     6     =  .   [     �  	   �     �     �     �     �     �     �  %     (   1  <   Z     �     �     �     �  
   �     �  "   �          /     @     S     V     ^     j     {  !   �     �     �     �     �  
   �     �  	     	     +   )  !   U     w  
   �     �     �     �  
   �     �     �     �     �               #     0     7     J     ]     i  4   �     3      -       )          A   	   4          $                                              :       
         "   E              ,   /      9      &   0       C   2   B             =   8   .                        '                D                   ?   !   7   #   1       @       +   %           (   <      5   >   6              *              F   ;    A URL is required. A name is required. About Add Add to... Added Albums Artist Not Found Artists Cancel Clear Community Playlists Could not Import Playlist Could not Rename Create Delete Deleted "{playlist_name}" Download Downloaded Duplicate Editable Enter Playlist Name... Enter Playlist URL... Failed to retrieve playlist data from server. Go back Import Import playlist... Import... Library More actions New Playlist Name... Next song No Results Normal Playback Ok Other Play Play all Playback mode Playlist already exists Playlists Previous song Queue Queue Empty Radio Mode Recently Played Recommended Remove Remove From Downloads Remove From Playlist Remove From Queue Rename Rename... Repeat Song Search Search... Show all Shuffle Songs Synchronized Toggle pause Top Result Undo Videos View Artist View artist Volume Your Playlists translator-credits Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.4.2
 Eine Adresse ist erforderlich. Ein Name ist erforderlich. Info Hinzufügen Hinzufügen zu … Hinzugefügt Alben Interpret nicht gefunden Interpreten Abbrechen Leeren Gemeinschaftswiedergabelisten Wiedergabeliste konnte nicht importiert werden Umbenennen nicht möglich Erstellen Löschen »{playlist_name}« gelöscht Herunterladen Heruntergeladen Duplizieren Bearbeitbar Name der Wiedergabeliste eingeben … Adresse der Wiedergabeliste eingeben … Abrufen der Wiedergabelistendaten vom Server fehlgeschlagen. Zurückgehen Importieren Wiedergabeliste importieren … Importieren … Bibliothek Weitere Aktionen Name der neuen Wiedergabeliste … Nächster Titel Keine Ergebnisse Normale Wiedergabe OK Weitere Wiedergeben Alle wiedergeben Wiedergabemodus Wiedergabeliste existiert bereits Wiedergabelisten Vorheriger Titel Warteschlange Warteschlange ist leer Radiomodus Zuletzt wiedergegeben Empfohlen Entfernen Aus den heruntergeladenen Dateien entfernen Aus der Wiedergabeliste entfernen Aus der Warteschlange entfernen Umbenennen Umbenennen … Titel wiederholen Suchen Suchen … Alle anzeigen Durchmischen Titel Abgeglichen Wiedergabe/Pause Bestes Ergebnis Rückgängig Videos Interpret anzeigen Interpret anzeigen Lautstärke Eigene Wiedergabelisten Jürgen Benvenuti <gastornis@posteo.org>, 2023, 2024 