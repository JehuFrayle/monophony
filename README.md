Monophony is a free and open source Linux app for streaming music from YouTube. It has no ads and does not require an account.

Copyright © 2022-present Zehkira, [AGPLv3-or-later](https://gitlab.com/zehkira/monophony/-/blob/master/source/LICENSE). [Metadata](https://gitlab.com/zehkira/monophony/-/blob/master/source/data/metainfo.xml) provided under the [0BSD](https://opensource.org/license/0bsd/) license.

| [Install](https://gitlab.com/zehkira/monophony/-/blob/master/INSTALL.md) | [Donate](https://gitlab.com/zehkira/zehkira/-/blob/master/DONATE.md) |
|-|-|

<img src='https://gitlab.com/zehkira/monophony/-/raw/master/assets/screenshot1.png' alt='screenshot'>
